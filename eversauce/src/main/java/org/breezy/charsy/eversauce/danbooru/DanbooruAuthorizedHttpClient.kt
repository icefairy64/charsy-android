package org.breezy.charsy.eversauce.danbooru

import org.breezy.charsy.eversauce.HttpClient
import kotlin.reflect.KClass
import kotlin.reflect.KType

class DanbooruAuthorizedHttpClient<T>(
    private val impl: HttpClient<T>,
    private val secrets: DanbooruSecrets
) : HttpClient<T> {
    override suspend fun get(url: String): Result<T> = impl.get(url, listOf(Pair("login", secrets.login), Pair("api_key", secrets.apiKey)))
    override suspend fun get(url: String, queryParams: List<Pair<String, String>>): Result<T> {
        return super.get(url, queryParams + listOf(Pair("login", secrets.login), Pair("api_key", secrets.apiKey)))
    }
}