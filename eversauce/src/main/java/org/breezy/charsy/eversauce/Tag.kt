package org.breezy.charsy.eversauce

interface Tag {
    val shortName: String
    val qualifiedName: String
}

interface CategorizedTag : Tag {
    val category: TagCategory
}

enum class TagCategory {
    General,
    Character,
    Copyright,
    Artist,
    Meta
}