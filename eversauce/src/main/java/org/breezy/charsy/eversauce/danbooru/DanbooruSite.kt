package org.breezy.charsy.eversauce.danbooru

import org.breezy.charsy.eversauce.*
import java.util.*

data class DanbooruSecrets(
    val login: String,
    val apiKey: String
)

class DanbooruSite(
    override val id: UUID,
    override val name: String,
    val urlRoot: String,
    val http: DanbooruHttpDirectory,
    private val authPrompter: suspend () -> Result<DanbooruSecrets>
) : GallerySite, AuthenticatableSite, SiteWithFavourites, SiteWithPersonalCollections {

    private var secrets: DanbooruSecrets? = null

    private fun <T> getEffectiveHttpClient(http: HttpClient<T>): HttpClient<T> =
        secrets?.let { DanbooruAuthorizedHttpClient(http, it) } ?: http

    override val mainFeed: ItemFeed<Post>
        get() = DanbooruIndexFeed(urlRoot, getEffectiveHttpClient(http.postList), id)

    override val isLoggedIn: Boolean
        get() = secrets != null

    override val userName: String?
        get() = secrets?.login

    override suspend fun logIn(): Result<Boolean> = authPrompter().map {
        secrets = it
        return@map true
    }

    override suspend fun ensureAuthentication(): Result<Boolean> {
        TODO("Not yet implemented")
    }

    override val favouritesFeed: ItemFeed<Post>
        get() = TODO("Not yet implemented")

    override suspend fun isPostFavourite(post: Post): Result<Boolean> {
        TODO("Not yet implemented")
    }

    override suspend fun favouritePost(post: Post): Result<Boolean> {
        TODO("Not yet implemented")
    }

    override suspend fun unfavouritePost(post: Post): Result<Boolean> {
        TODO("Not yet implemented")
    }

    override val personalPostCollections: ItemFeed<SitePostCollection>
        get() = TODO("Not yet implemented")

    override suspend fun getPersonalCollectionsForPost(post: Post): Result<List<SitePostCollection>> {
        TODO("Not yet implemented")
    }

    override suspend fun addPostToCollection(
        post: Post,
        collection: SitePostCollection
    ): Result<Boolean> {
        TODO("Not yet implemented")
    }

    override suspend fun removePostFromCollection(
        post: Post,
        collection: SitePostCollection
    ): Result<Boolean> {
        TODO("Not yet implemented")
    }
}