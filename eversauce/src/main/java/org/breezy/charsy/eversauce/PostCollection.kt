package org.breezy.charsy.eversauce

import java.util.UUID

interface PostCollection {
    val name: String
    val feed: ItemFeed<Post>
}

interface SitePostCollection : PostCollection {
    val siteId: UUID
    val ownerUserName: String
}