package org.breezy.charsy.eversauce

import java.util.UUID

interface User {
    val siteId: UUID
    val name: String
    val profilePictureUrl: String?
}