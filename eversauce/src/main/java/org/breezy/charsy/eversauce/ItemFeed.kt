package org.breezy.charsy.eversauce

interface ItemFeed<TItem> {
    suspend fun getPage(pageToken: FeedPageToken?): Result<FeedPage<TItem>>
}

interface FeedPageToken {
    val stringValue: String
}

interface FeedPage<TItem> {
    val items: List<TItem>
    val nextPageToken: FeedPageToken?
    val prevPageToken: FeedPageToken?
}