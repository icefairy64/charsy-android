package org.breezy.charsy.eversauce.danbooru

import org.breezy.charsy.eversauce.PostAttachment
import org.breezy.charsy.eversauce.Tag
import org.breezy.charsy.eversauce.TagCategory
import java.util.*

data class DanbooruApiPost(
    val id: Int,
    val rating: Char,
    val imageWidth: Int,
    val imageHeight: Int,
    val isPending: Boolean,
    val fileExt: String,
    val fileUrl: String?,
    val largeFileUrl: String?,
    val previewFileUrl: String?,
    val parentId: Int?,
    val hasChildren: Boolean,
    val tagString: String,
    val tagStringGeneral: String,
    val tagStringMeta: String,
    val tagStringCopyright: String,
    val tagStringArtist: String,
    val tagStringCharacter: String,
    val source: String?,
    val score: Int,
    val createdAt: String,
    val updatedAt: String
)

fun DanbooruApiPost.toDomainPost(siteId: UUID): DanbooruPost {
    val attachments = mutableListOf<DanbooruImagePostAttachment>()
    if (fileUrl != null) {
        attachments.add(DanbooruImagePostAttachment(PostAttachment.SOURCE_QUALITY_WEIGHT, fileUrl, null, imageWidth, imageHeight))
    }
    if (largeFileUrl != null) {
        attachments.add(DanbooruImagePostAttachment(PostAttachment.PREVIEW_QUALITY_WEIGHT, largeFileUrl, null, null, null))
    }
    if (previewFileUrl != null) {
        attachments.add(DanbooruImagePostAttachment(PostAttachment.THUMBNAIL_QUALITY_WEIGHT, previewFileUrl, null, null, null))
    }

    val tags = mutableListOf<Tag>()
    tagStringArtist.split(" ")
        .map { DanbooruTag(it, TagCategory.Artist)
        }.forEach { tags.add(it) }

    tagStringCharacter.split(" ")
        .map { DanbooruTag(it, TagCategory.Character)
        }.forEach { tags.add(it) }

    tagStringCopyright.split(" ")
        .map { DanbooruTag(it, TagCategory.Copyright)
        }.forEach { tags.add(it) }

    tagStringMeta.split(" ")
        .map { DanbooruTag(it, TagCategory.Meta)
        }.forEach { tags.add(it) }

    tagStringGeneral.split(" ")
        .map { DanbooruTag(it, TagCategory.General)
        }.forEach { tags.add(it) }

    return DanbooruPost(
        siteId = siteId,
        id = DanbooruPostId(id),
        tags = tags,
        approved = !isPending,
        rating = DanbooruPostRating.fromShortName(rating),
        title = null,
        description = null,
        submittedBy = null,
        score = score,
        artistName = tagStringArtist.split(" ").first(),
        canBeDetailed = true,
        hasParent = parentId != null,
        hasChildren = hasChildren,
        childCount = null,
        attachments = attachments
    )
}