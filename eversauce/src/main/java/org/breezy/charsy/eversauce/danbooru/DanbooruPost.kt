package org.breezy.charsy.eversauce.danbooru

import org.breezy.charsy.eversauce.*
import java.util.UUID

class DanbooruPost(
    override val siteId: UUID,
    override val id: DanbooruPostId,
    override val tags: List<Tag>,
    override val approved: Boolean?,
    override val rating: PostRating?,
    override val title: String?,
    override val description: String?,
    override val submittedBy: User?,
    override val score: Int?,
    override val artistName: String?,
    override val attachments: List<PostAttachment>,
    override val canBeDetailed: Boolean,
    override val hasParent: Boolean,
    override val hasChildren: Boolean,
    override val childCount: Short?
) : Post, ContentfulPost, HierarchicalPost, DetailablePost {
    override suspend fun detail(): Post {
        TODO("Not yet implemented")
    }

    override suspend fun getParent(): Post? {
        TODO("Not yet implemented")
    }

    override suspend fun getChildren(): List<Post> {
        TODO("Not yet implemented")
    }

}

class DanbooruPostId(val id: Int) : PostId {
    override val stringValue: String
        get() = id.toString()
}

class DanbooruTag(override val shortName: String, override val category: TagCategory) : CategorizedTag {
    override val qualifiedName: String
        get() = shortName
}

class DanbooruPostRating(
    override val shortName: String,
    override val fullName: String,
    val weight: Byte
) : PostRating {
    override fun compareTo(other: PostRating): Int {
        return when (other) {
            is DanbooruPostRating -> weight - other.weight
            is CommonPostRating -> weight - other.weight
            else -> throw Error()
        }
    }

    companion object {
        val GENERAL = DanbooruPostRating("g", "general", CommonPostRating.SAFE.weight)
        val SENSITIVE = DanbooruPostRating("s", "sensitive", 16)
        val QUESTIONABLE = DanbooruPostRating("q", "questionable", CommonPostRating.QUESTIONABLE.weight)
        val EXPLICIT = DanbooruPostRating("e", "explicit", CommonPostRating.EXPLICIT.weight)

        fun fromShortName(shortName: Char) =
            when (shortName) {
                'g' -> GENERAL
                's' -> SENSITIVE
                'q' -> QUESTIONABLE
                'e' -> EXPLICIT
                else -> throw Error("Unsupported Danbooru rating $shortName")
            }
    }
}

class DanbooruImagePostAttachment(
    override val qualityWeight: Byte,
    override val url: String,
    override val size: Long?,
    override val width: Int?,
    override val height: Int?
) : VisualPostAttachment {
    val extension: String
        get() = url.substring(url.lastIndexOf('.') + 1)

    override val mediaType: MediaType
        get() = when (extension) {
            "jpg", "jpeg" -> DefaultMediaType.image("jpeg")
            "png" -> DefaultMediaType.image("png")
            "gif" -> DefaultMediaType.image("gif")
            "mp4" -> DefaultMediaType.video("mp4")
            "webm" -> DefaultMediaType.video("webm")
            "webp" -> DefaultMediaType.video("webp")
            else -> throw Error("Unknown extension $extension")
        }
}