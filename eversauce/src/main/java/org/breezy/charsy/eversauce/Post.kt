package org.breezy.charsy.eversauce

import java.util.UUID
import kotlin.time.Duration

interface Post {
    val siteId: UUID
    val id: PostId
    val tags: List<Tag>
    val approved: Boolean?
    val rating: PostRating?
    val title: String?
    val description: String?
    val submittedBy: User?
    val score: Int?
    val artistName: String?
}

interface PostId {
    val stringValue: String
}

interface CommentedPost : Post {
    suspend fun getComments(): List<PostComment>
}

interface PostComment {
    val submittedBy: User
    val body: String
}

interface ContentfulPost : Post {
    val attachments: List<PostAttachment>

    val source: PostAttachment?
        get() = getQualityAttachment(PostAttachment.SOURCE_QUALITY_WEIGHT)

    val thumbnail: PostAttachment?
        get() = getQualityAttachment(PostAttachment.THUMBNAIL_QUALITY_WEIGHT)

    val preview: PostAttachment?
        get() = getQualityAttachment(PostAttachment.PREVIEW_QUALITY_WEIGHT)

    fun getQualityAttachment(quality: Byte) =
        attachments.sortedBy { it.qualityWeight }.find { it.qualityWeight >= quality }
}

interface PostAttachment {
    val qualityWeight: Byte
    val mediaType: MediaType
    val url: String
    val size: Long?

    companion object {
        val THUMBNAIL_QUALITY_WEIGHT: Byte = 32
        val PREVIEW_QUALITY_WEIGHT: Byte = 64
        val SOURCE_QUALITY_WEIGHT: Byte = Byte.MAX_VALUE
    }
}

interface VisualPostAttachment : PostAttachment {
    val width: Int?
    val height: Int?
}

interface VideoPostAttachment : VisualPostAttachment {
    val duration: Duration?
}

interface MediaType {
    val type: String
    val subtype: String
    val stringValue: String
        get() = "${type}/${subtype}"
}

data class DefaultMediaType(override val type: String, override val subtype: String) : MediaType {
    companion object {
        fun image(subtype: String): MediaType = DefaultMediaType("image", subtype)
        fun video(subtype: String): MediaType = DefaultMediaType("video", subtype)
    }
}

interface DetailablePost : Post {
    val canBeDetailed: Boolean
    suspend fun detail(): Post
}

interface HierarchicalPost : Post {
    val hasParent: Boolean
    val hasChildren: Boolean
    val childCount: Short?
    suspend fun getParent() : Post?
    suspend fun getChildren() : List<Post>
}

interface SourcedPost : Post {
    val originalSourceUrl: String?
}