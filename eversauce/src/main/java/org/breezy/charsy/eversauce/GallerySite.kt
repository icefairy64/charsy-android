package org.breezy.charsy.eversauce

import java.util.UUID

interface GallerySite {
    val id: UUID
    val name: String
    val mainFeed: ItemFeed<Post>
}

interface SearchableSite : GallerySite {
    fun search(query: String): Result<ItemFeed<Post>>
}

interface AuthenticatableSite : GallerySite {
    val isLoggedIn: Boolean
    val userName: String?

    suspend fun logIn(): Result<Boolean>
    suspend fun ensureAuthentication(): Result<Boolean>
}

interface SiteWithFavourites : AuthenticatableSite {
    val favouritesFeed: ItemFeed<Post>
    suspend fun isPostFavourite(post: Post): Result<Boolean>
    suspend fun favouritePost(post: Post): Result<Boolean>
    suspend fun unfavouritePost(post: Post): Result<Boolean>
}

interface SiteWithPersonalCollections : AuthenticatableSite {
    val personalPostCollections: ItemFeed<SitePostCollection>
    suspend fun getPersonalCollectionsForPost(post: Post): Result<List<SitePostCollection>>
    suspend fun addPostToCollection(post: Post, collection: SitePostCollection): Result<Boolean>
    suspend fun removePostFromCollection(post: Post, collection: SitePostCollection): Result<Boolean>
}