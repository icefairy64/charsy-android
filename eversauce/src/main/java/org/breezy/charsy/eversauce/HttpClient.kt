package org.breezy.charsy.eversauce

import kotlin.reflect.KClass
import kotlin.reflect.KType

interface HttpClient<T> {
    suspend fun get(url: String): Result<T>
    suspend fun get(url: String, queryParams: List<Pair<String, String>>): Result<T> =
        get(url + with(queryParams.joinToString("&") { "${it.first}=${it.second}" }) { if (isNotEmpty()) "?$this" else "" })
}