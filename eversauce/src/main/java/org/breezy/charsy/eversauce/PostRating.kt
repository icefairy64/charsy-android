package org.breezy.charsy.eversauce

interface PostRating : Comparable<PostRating> {
    val shortName: String
    val fullName: String
}

class CommonPostRating(override val shortName: String, override val fullName: String, val weight: Byte) : PostRating {
    override fun compareTo(other: PostRating): Int {
        return when (other) {
            is CommonPostRating -> weight - other.weight;
            else -> if (SAFE >= other) compareTo(SAFE)
                else if (QUESTIONABLE >= other) compareTo(QUESTIONABLE)
                else if (EXPLICIT >= other) compareTo(EXPLICIT)
                else 1
        }
    }

    companion object {
        val SAFE: CommonPostRating = CommonPostRating("s", "safe", 0)
        val QUESTIONABLE: CommonPostRating = CommonPostRating("q", "questionable", 32)
        val EXPLICIT: CommonPostRating = CommonPostRating("e", "explicit", 64)
    }
}