package org.breezy.charsy.eversauce.danbooru

import org.breezy.charsy.eversauce.HttpClient

interface DanbooruHttpDirectory {
    val postList: HttpClient<List<DanbooruApiPost>>
}