package org.breezy.charsy.eversauce.danbooru

import org.breezy.charsy.eversauce.*
import java.util.UUID

class DanbooruIndexFeed(
    private val urlRoot: String,
    private val http: HttpClient<List<DanbooruApiPost>>,
    private val siteId: UUID
) : ItemFeed<Post> {

    override suspend fun getPage(pageToken: FeedPageToken?): Result<FeedPage<Post>> {
        val url = "$urlRoot/posts.json"
        return http.get(url, listOf(Pair("page", when (pageToken) { is DanbooruPageToken -> pageToken.anchor else -> "1" })))
            .map {
                val prevPageToken = if (it.isNotEmpty() && pageToken != null) { DanbooruPageToken("a${it.first().id}") } else null
                val nextPageToken = if (it.isNotEmpty()) { DanbooruPageToken("b${it.last().id}") } else null
                return@map DanbooruPostFeedPage(it.map { x -> x.toDomainPost(siteId) }, nextPageToken, prevPageToken)
            }
    }
}

class DanbooruPageToken(
    val anchor: String
) : FeedPageToken {
    override val stringValue: String
        get() = anchor
}

class DanbooruPostFeedPage(
    override val items: List<Post>,
    override val nextPageToken: FeedPageToken?,
    override val prevPageToken: FeedPageToken?
) : FeedPage<Post>