package org.breezy.charsy.eversauce

import org.breezy.charsy.eversauce.danbooru.DanbooruApiPost
import org.breezy.charsy.eversauce.danbooru.DanbooruHttpDirectory

class MockHttpDirectory(
    override val postList: HttpClient<List<DanbooruApiPost>>
) : DanbooruHttpDirectory