package org.breezy.charsy.eversauce

import io.kotest.core.spec.style.FunSpec
import io.kotest.inspectors.forExactly
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.result.shouldBeSuccess
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeInstanceOf
import org.breezy.charsy.eversauce.danbooru.DanbooruApiPost
import org.breezy.charsy.eversauce.danbooru.DanbooruPageToken
import org.breezy.charsy.eversauce.danbooru.DanbooruPostRating
import org.breezy.charsy.eversauce.danbooru.DanbooruSite
import java.util.*

class TestDanbooru : FunSpec({
    val urlRoot = "https://testbooru.donmai.us"

    val post1 = DanbooruApiPost(
        id = 1,
        rating = 'q',
        imageHeight = 1000,
        imageWidth = 1000,
        isPending = true,
        fileExt = "jpg",
        fileUrl = "$urlRoot/image.jpg",
        largeFileUrl = "$urlRoot/image.jpg",
        previewFileUrl = "$urlRoot/image.jpg",
        parentId = null,
        hasChildren = false,
        tagString = "1girl abs",
        tagStringGeneral = "",
        tagStringMeta = "",
        tagStringCopyright = "",
        tagStringArtist = "me",
        tagStringCharacter = "my",
        source = null,
        score = 12,
        createdAt = "",
        updatedAt = ""
    )

    test("single post on a single page") {
        val postListHttp = MockHttpClient<List<DanbooruApiPost>>().apply {
            expectRequest("$urlRoot/posts.json", listOf("page" to "1"), listOf(post1))
        }

        val http = MockHttpDirectory(
            postList = postListHttp
        )

        val site = DanbooruSite(
            id = UUID.randomUUID(),
            urlRoot = urlRoot,
            name = "testbooru",
            http = http,
            authPrompter = suspend { Result.failure(NotImplementedError()) }
        )

        site.mainFeed.getPage(null)
            .shouldBeSuccess { page ->
                page.items should { posts ->
                    posts.forExactly(1) { post ->
                        post.rating.shouldBe(DanbooruPostRating.QUESTIONABLE)
                        post.shouldBeInstanceOf<ContentfulPost>()
                        post.source.shouldNotBeNull()
                        post.preview.shouldNotBeNull()
                        post.thumbnail.shouldNotBeNull()
                    }
                }
                page.nextPageToken.shouldBeInstanceOf<DanbooruPageToken>()
                    .should { token ->
                        token.anchor.shouldBe(2)
                    }
                page.prevPageToken.shouldBeNull()
            }

        postListHttp.expectNoUnmatchedRequests()
    }
})