package org.breezy.charsy.eversauce

import io.kotest.matchers.maps.shouldBeEmpty
import io.kotest.matchers.nulls.shouldNotBeNull
import kotlin.reflect.KClass
import kotlin.reflect.KType
import kotlin.reflect.javaType
import kotlin.reflect.typeOf

class MockHttpClient<T> : HttpClient<T> {
    val expectedRequests: MutableMap<Pair<String, List<Pair<String, String>>>, T> = HashMap()

    override suspend fun get(url: String): Result<T> {
        TODO("Not yet implemented")
    }

    override suspend fun get(
        url: String,
        queryParams: List<Pair<String, String>>
    ): Result<T> {
        val response = expectedRequests.remove(Pair(url, queryParams.sortedBy { it.first }))
            .shouldNotBeNull()
        return Result.success(response)
    }

    fun expectRequest(url: String, queryParams: List<Pair<String, String>>, response: T) {
        expectedRequests[Pair(url, queryParams.sortedBy { it.first })] = response
    }

    fun expectNoUnmatchedRequests() {
        expectedRequests.shouldBeEmpty()
    }
}