package org.breezy.charsy.android

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.items
import coil.compose.AsyncImage
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import org.breezy.charsy.android.danbooru.DanbooruFeedService
import org.breezy.charsy.android.data.CharsyDatabase
import org.breezy.charsy.android.data.PostAttachmentEntity
import org.breezy.charsy.android.data.PostWithTagsAndAttachments
import org.breezy.charsy.android.paging.SitePostFeedRemoteMediator
import org.breezy.charsy.eversauce.*
import org.breezy.charsy.eversauce.danbooru.DanbooruSite
import javax.inject.Inject

@Composable
fun DanbooruTestView(
    modifier: Modifier,
    innerPadding: PaddingValues,
    viewModel: DanbooruTestViewModel = viewModel()
) {
    val posts = viewModel.postFlow.collectAsLazyPagingItems()
    Column(modifier = modifier.fillMaxWidth()) {
        Text(text = "${posts.itemCount} ${posts.loadState.refresh} ${posts.loadState.append} ${posts.loadState.prepend}")
        LazyColumn(verticalArrangement = Arrangement.spacedBy(8.dp), contentPadding = innerPadding) {
            items(items = posts) {
                Row(modifier = Modifier.fillMaxWidth()) {
                    when (it) {
                        null -> Text(text = "No post")
                        else -> Post(post = it)
                    }
                }
            }
        }
    }
}

@Composable
fun Post(post: PostWithTagsAndAttachments) {
    Box {
        Thumbnail(post = post)
        post.tags.filter { t -> t.category == TagCategory.Character }
            .take(1)
            .forEach {
                TagPill(tag = it.qualifiedName, modifier = Modifier
                    .align(Alignment.BottomEnd)
                    .offset((-8).dp, (-8).dp))
            }
    }
}

@Composable
fun Thumbnail(post: PostWithTagsAndAttachments) {
    val sourceAttachment = post.attachments.asSequence().sortedByDescending { it.qualityWeight }.firstOrNull()
    val thumbAttachment = post.attachments.asSequence().sortedBy { it.qualityWeight }.firstOrNull()
    if (sourceAttachment == null) {
        return Text(text = "No image")
    }
    AsyncImage(
        model = (thumbAttachment ?: sourceAttachment)?.url ?: throw Exception("No image URL"),
        contentDescription = "thumb",
        placeholder = PlaceholderPainter(sourceAttachment),
        modifier = Modifier
            .fillMaxWidth()
            .aspectRatio(
                (sourceAttachment.width ?: 1).toFloat() / (sourceAttachment.height
                    ?: 1).toFloat()
            ),
        contentScale = ContentScale.FillBounds,
    )
}

@Composable
fun TagPill(tag: String, modifier: Modifier) {
    Surface(
        modifier = modifier,
        shape = MaterialTheme.shapes.small,
        color = MaterialTheme.colorScheme.secondary,
        tonalElevation = 1.dp,
        shadowElevation = 1.dp
    ) {
        Text(text = tag, modifier = Modifier.padding(4.dp), color = MaterialTheme.colorScheme.onSecondary)
    }
}

class PlaceholderPainter(private val attachment: PostAttachmentEntity) : Painter() {
    override val intrinsicSize: Size
        get() = Size((attachment.width ?: 100).toFloat(), (attachment.height ?: 100).toFloat())

    override fun DrawScope.onDraw() {
        drawRect(Color(200, 150, 70))
    }
}

@HiltViewModel
class DanbooruTestViewModel @Inject constructor(
    private val site: DanbooruSite,
    private val db: CharsyDatabase
) : ViewModel() {
    val stateFlow: StateFlow<DanbooruTestViewState>
        get() = mutableStateFlow

    private val mutableStateFlow: MutableStateFlow<DanbooruTestViewState> = MutableStateFlow(DanbooruTestViewState(listOf(), "No error"))

    @OptIn(ExperimentalPagingApi::class)
    val postFlow: Flow<PagingData<PostWithTagsAndAttachments>> = Pager(
        config = PagingConfig(20),
        remoteMediator = SitePostFeedRemoteMediator(DanbooruFeedService(site.mainFeed, "danbooru-main-feed"), db)
    ) {
        db.feedDao().getPostsInFeed("danbooru-main-feed")
    }.flow.cachedIn(viewModelScope)
}

data class DanbooruTestViewState(val posts: List<Post>, val error: String = "No error")