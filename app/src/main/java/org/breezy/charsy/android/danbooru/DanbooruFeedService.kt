package org.breezy.charsy.android.danbooru

import org.breezy.charsy.android.site.SiteFeedPage
import org.breezy.charsy.android.site.SiteFeedService
import org.breezy.charsy.eversauce.ItemFeed
import org.breezy.charsy.eversauce.Post
import org.breezy.charsy.eversauce.danbooru.DanbooruPageToken

class DanbooruFeedService(private val feed: ItemFeed<Post>, override val feedId: String) : SiteFeedService {
    override suspend fun fetch(pageToken: String?): Result<SiteFeedPage> {
        val danbooruToken = pageToken?.let { DanbooruPageToken(pageToken) }
        return feed.getPage(danbooruToken)
            .map { SiteFeedPage(
                posts = it.items,
                nextPageToken = it.nextPageToken?.stringValue,
                prevPageToken = it.prevPageToken?.stringValue
            ) }
    }
}