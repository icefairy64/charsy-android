package org.breezy.charsy.android.data

import androidx.paging.PagingSource
import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import androidx.room.Query

@Entity(tableName = "feed_to_post", primaryKeys = ["feed_id", "post_id"])
data class FeedToPostCrossRef(
    @ColumnInfo("feed_id")
    val feedId: String,

    @ColumnInfo("post_id")
    val postId: Long,

    @ColumnInfo("feed_post_order")
    val feedPostOrder: Long
)

@Entity("feed_paging_keys")
data class FeedPagingKey(
    @PrimaryKey
    @ColumnInfo("feed_id")
    val feedId: String,

    @ColumnInfo("prev_key")
    val prevKey: String?,

    @ColumnInfo("next_key")
    val nextKey: String?
)

@Dao
interface FeedDao {
    @Query("SELECT * FROM posts p JOIN feed_to_post ftp ON p.id = ftp.post_id WHERE ftp.feed_id = :feedId ORDER BY ftp.feed_post_order ASC")
    fun getPostsInFeed(feedId: String): PagingSource<Int, PostWithTagsAndAttachments>

    @Query("SELECT count(*) FROM feed_to_post WHERE feed_id = :feedId")
    fun getPostCountInFeed(feedId: String): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(posts: List<FeedToPostCrossRef>)

    @Query("DELETE FROM feed_to_post WHERE feed_id = :feedId")
    fun deleteFeed(feedId: String)
}

@Dao
interface FeedPagingKeyDao {
    @Query("SELECT * FROM feed_paging_keys WHERE feed_id = :feedId")
    fun get(feedId: String): FeedPagingKey?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(key: FeedPagingKey)

    @Query("DELETE FROM feed_paging_keys WHERE feed_id = :feedId")
    fun delete(feedId: String)
}