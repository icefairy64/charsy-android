package org.breezy.charsy.android

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CharsyApplication : Application()