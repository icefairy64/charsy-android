package org.breezy.charsy.android.data

import androidx.paging.PagingSource
import androidx.room.*
import org.breezy.charsy.eversauce.CategorizedTag
import org.breezy.charsy.eversauce.ContentfulPost
import org.breezy.charsy.eversauce.Post
import org.breezy.charsy.eversauce.TagCategory
import org.breezy.charsy.eversauce.VisualPostAttachment

@Entity(tableName = "posts", indices = [Index(value = ["source_name", "source_local_id"], unique = true)])
data class PostEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Long,

    @ColumnInfo("source_name")
    val sourceName: String,

    @ColumnInfo("source_local_id")
    val sourceLocalId: String,

    @ColumnInfo("source_order")
    val sourceOrder: Long
)

@Entity(tableName = "attachments")
data class PostAttachmentEntity(
    @PrimaryKey
    val url: String,

    @ColumnInfo("content_type")
    val contentType: String?,

    @ColumnInfo("quality_weight")
    val qualityWeight: Int,

    val width: Int?,
    val height: Int?,

    @ColumnInfo("file_size")
    val fileSize: Long?
)

@Entity(tableName = "post_to_attachment", primaryKeys = ["post_id", "attachment_url"])
data class PostToAttachmentCrossRef(
    @ColumnInfo("post_id")
    val postId: Long,

    @ColumnInfo("attachment_url")
    val attachmentUrl: String
)

@Entity(tableName = "tags")
data class PostTag(
    @PrimaryKey
    @ColumnInfo("qualified_name")
    val qualifiedName: String,

    val category: TagCategory?
)

@Entity(tableName = "post_to_tag", primaryKeys = ["post_id", "tag_name"])
data class PostToTagCrossRef(
    @ColumnInfo("post_id")
    val postId: Long,

    @ColumnInfo("tag_name")
    val tagName: String
)

data class PostWithTagsAndAttachments (
    @Embedded
    val post: PostEntity,

    @Relation(
        parentColumn = "id",
        entityColumn = "qualified_name",
        associateBy = Junction(PostToTagCrossRef::class,
            parentColumn = "post_id",
            entityColumn = "tag_name")
    )
    val tags: List<PostTag>,

    @Relation(
        parentColumn = "id",
        entityColumn = "url",
        associateBy = Junction(PostToAttachmentCrossRef::class,
            parentColumn = "post_id",
            entityColumn = "attachment_url")
    )
    val attachments: List<PostAttachmentEntity>
)

@Dao
abstract class PostDao {
    @Query("SELECT * FROM posts")
    abstract fun getPosts(): PagingSource<Int, PostWithTagsAndAttachments>

    @Query("SELECT * FROM posts WHERE rowid = :rowId")
    abstract fun getPostByRowId(rowId: Long): PostEntity

    @Query("SELECT * FROM posts p JOIN post_to_tag ptt ON p.id = ptt.post_id JOIN tags t ON t.qualified_name = ptt.tag_name WHERE t.qualified_name = :tag")
    abstract fun getPostsByTag(tag: String): PagingSource<Int, PostWithTagsAndAttachments>

    @Query("SELECT * FROM tags WHERE qualified_name = :tag")
    abstract fun getTagByName(tag: String): PostTag?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertPost(post: PostEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAllPosts(posts: List<PostEntity>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insertAllAttachments(attachments: List<PostAttachmentEntity>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insertAllPostToAttachments(attachments: List<PostToAttachmentCrossRef>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insertAllTags(tags: List<PostTag>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insertAllPostToTags(tags: List<PostToTagCrossRef>)

    @Query("DELETE FROM posts")
    abstract fun deleteAllPosts()

    fun insertAll(posts: List<Post>): List<Long> {
        return posts.map { post ->
            val dbTags = post.tags.map { PostTag(it.qualifiedName, (it as? CategorizedTag)?.category) }
            val dbAttachments = (post as? ContentfulPost)?.attachments?.map { a -> PostAttachmentEntity(
                url = a.url,
                contentType = a.mediaType.stringValue,
                qualityWeight = a.qualityWeight.toInt(),
                width = (a as? VisualPostAttachment)?.width,
                height = (a as? VisualPostAttachment)?.height,
                fileSize = a.size
            )} ?: listOf()
            val dbPost = PostEntity(
                id = 0,
                sourceName = post.siteId.toString(),
                sourceLocalId = post.id.stringValue,
                sourceOrder = 0
            )
            insertAllTags(dbTags)
            insertAllAttachments(dbAttachments)
            val dbPostRowId = insertPost(dbPost)

            insertAllPostToTags(dbTags.map { PostToTagCrossRef(
                postId = getPostByRowId(dbPostRowId).id,
                tagName = it.qualifiedName
            ) })

            insertAllPostToAttachments(dbAttachments.map { PostToAttachmentCrossRef(
                postId = getPostByRowId(dbPostRowId).id,
                attachmentUrl = it.url
            ) })

            return@map dbPostRowId
        }
    }
}