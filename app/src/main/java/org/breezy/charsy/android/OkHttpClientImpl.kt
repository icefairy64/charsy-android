package org.breezy.charsy.android

import com.squareup.moshi.JsonAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import okhttp3.Request
import org.breezy.charsy.eversauce.HttpClient

class OkHttpClientImpl<T>(
    val moshiAdapter: JsonAdapter<T>
) : HttpClient<T> {
    private val http: OkHttpClient = OkHttpClient()

    override suspend fun get(url: String): Result<T> {
        val request = Request.Builder()
            .url(url)
            .build()

        return withContext(Dispatchers.IO) {
            kotlin.runCatching {
                val response = http.newCall(request).execute()
                response.body?.source()?.let { moshiAdapter.fromJson(it) } ?: throw Exception("")
            }
        }
    }
}

class OkHttpMappingClientImpl<T, R>(
    val moshiAdapter: JsonAdapter<T>,
    val mapper: (T) -> R
) : HttpClient<R> {
    private val http: OkHttpClient = OkHttpClient()

    override suspend fun get(url: String): Result<R> {
        val request = Request.Builder()
            .url(url)
            .build()

        return withContext(Dispatchers.IO) {
            kotlin.runCatching {
                val response = http.newCall(request).execute()
                response.body?.source()?.let { mapper(moshiAdapter.fromJson(it) ?: throw Exception("No value")) } ?: throw Exception("")
            }
        }
    }
}