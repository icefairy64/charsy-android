package org.breezy.charsy.android

import android.content.Context
import androidx.room.Room
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import org.breezy.charsy.android.data.CharsyDatabase

@Module
@InstallIn(SingletonComponent::class)
class CharsyHiltModule {
    @Provides
    fun provideMoshi(): Moshi {
        return Moshi.Builder()
            .addLast(KotlinJsonAdapterFactory())
            .build()
    }

    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context): CharsyDatabase {
        return Room.databaseBuilder(appContext, CharsyDatabase::class.java, "Charsy")
            .build();
    }
}