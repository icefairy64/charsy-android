package org.breezy.charsy.android.data

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    entities = [
        PostEntity::class,
        PostAttachmentEntity::class,
        PostToAttachmentCrossRef::class,
        PostTag::class,
        PostToTagCrossRef::class,
        FeedToPostCrossRef::class,
        FeedPagingKey::class
    ],
    version = 2
)
abstract class CharsyDatabase : RoomDatabase() {
    abstract fun postDao(): PostDao
    abstract fun feedDao(): FeedDao
    abstract fun feedPagingKeyDao(): FeedPagingKeyDao
}