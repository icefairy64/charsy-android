package org.breezy.charsy.android.paging

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import org.breezy.charsy.android.data.CharsyDatabase
import org.breezy.charsy.android.data.FeedPagingKey
import org.breezy.charsy.android.data.FeedToPostCrossRef
import org.breezy.charsy.android.data.PostWithTagsAndAttachments
import org.breezy.charsy.android.site.SiteFeedService

@OptIn(ExperimentalPagingApi::class)
class SitePostFeedRemoteMediator(
    val feedService: SiteFeedService,
    val db: CharsyDatabase
) : RemoteMediator<Int, PostWithTagsAndAttachments>() {
    override suspend fun load(loadType: LoadType, state: PagingState<Int, PostWithTagsAndAttachments>): MediatorResult {
        return try {
            val loadKey = when (loadType) {
                LoadType.REFRESH -> null
                LoadType.PREPEND -> {
                    val remoteKey = db.withTransaction {
                        db.feedPagingKeyDao().get(feedService.feedId)?.prevKey
                    } ?: return MediatorResult.Success(true)

                    remoteKey
                }
                LoadType.APPEND -> {
                    val remoteKey = db.withTransaction {
                        db.feedPagingKeyDao().get(feedService.feedId)?.nextKey
                    } ?: return MediatorResult.Success(true)

                    remoteKey
                }
            }

            val page = feedService.fetch(loadKey).getOrThrow()

            db.withTransaction {
                if (loadType == LoadType.REFRESH) {
                    db.feedDao().deleteFeed(feedService.feedId)
                    db.feedPagingKeyDao().delete(feedService.feedId)
                }

                var index = db.feedDao().getPostCountInFeed(feedService.feedId)

                val dbFeedPosts = db.postDao().insertAll(page.posts).asSequence()
                    .map { rowId -> db.postDao().getPostByRowId(rowId) }
                    .map { dbPost -> FeedToPostCrossRef(
                        feedId = feedService.feedId,
                        postId = dbPost.id,
                        feedPostOrder = index++
                    ) }
                    .toList()

                db.feedDao().insertAll(dbFeedPosts)

                db.feedPagingKeyDao().insert(FeedPagingKey(
                    feedId = feedService.feedId,
                    prevKey = page.prevPageToken,
                    nextKey = page.nextPageToken
                ))
            }

            return MediatorResult.Success(page.nextPageToken == null)
        }
        catch (e: Exception) {
            return MediatorResult.Error(e)
        }
    }
}