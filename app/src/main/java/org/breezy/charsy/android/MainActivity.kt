package org.breezy.charsy.android

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.consumeWindowInsets
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import dagger.hilt.android.AndroidEntryPoint
import org.breezy.charsy.android.ui.theme.CharsyAndroidTheme

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CharsyAndroidTheme {
                MainContent()
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class, ExperimentalLayoutApi::class)
@Composable
fun MainContent() {
    Scaffold { innerPadding ->
        DanbooruTestView(Modifier.consumeWindowInsets(innerPadding), innerPadding)
    }
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name a!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    CharsyAndroidTheme {
        MainContent()
    }
}