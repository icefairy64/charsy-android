package org.breezy.charsy.android.danbooru

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import org.breezy.charsy.eversauce.danbooru.DanbooruHttpDirectory
import org.breezy.charsy.eversauce.danbooru.DanbooruSite
import java.util.*

@Module
@InstallIn(ViewModelComponent::class)
class DanbooruHiltModule {
    @Provides
    fun provideDanbooruSite(
        httpDirectory: OkHttpDanbooruAdapter
    ) : DanbooruSite = DanbooruSite(UUID.randomUUID(), "Danbooru", "https://danbooru.donmai.us", httpDirectory, suspend { throw Error() })
}