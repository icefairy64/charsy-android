package org.breezy.charsy.android.site

import org.breezy.charsy.eversauce.Post

interface SiteFeedService {
    val feedId: String
    suspend fun fetch(pageToken: String?): Result<SiteFeedPage>
}

data class SiteFeedPage (
    val posts: List<Post>,
    val nextPageToken: String?,
    val prevPageToken: String?
)