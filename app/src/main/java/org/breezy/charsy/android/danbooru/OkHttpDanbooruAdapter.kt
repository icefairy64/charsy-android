package org.breezy.charsy.android.danbooru

import com.squareup.moshi.Moshi
import com.squareup.moshi.adapter
import org.breezy.charsy.android.OkHttpClientImpl
import org.breezy.charsy.android.OkHttpMappingClientImpl
import org.breezy.charsy.eversauce.HttpClient
import org.breezy.charsy.eversauce.danbooru.DanbooruApiPost
import org.breezy.charsy.eversauce.danbooru.DanbooruHttpDirectory
import javax.inject.Inject

class OkHttpDanbooruAdapter @Inject constructor(
    val moshi: Moshi
) : DanbooruHttpDirectory {

    @OptIn(ExperimentalStdlibApi::class)
    override val postList: HttpClient<List<DanbooruApiPost>>
        get() {
            val adapter = moshi.adapter<List<MoshiDanbooruApiPost>>()
            return OkHttpMappingClientImpl(adapter) { it.map { p -> p.toApi() } }
        }
}

data class MoshiDanbooruApiPost(
    val id: Int,
    val rating: Char,
    val image_width: Int,
    val image_height: Int,
    val is_pending: Boolean,
    val file_ext: String,
    val file_url: String?,
    val large_file_url: String?,
    val preview_file_url: String?,
    val parent_id: Int?,
    val has_children: Boolean,
    val tag_string: String,
    val tag_string_general: String,
    val tag_string_meta: String,
    val tag_string_copyright: String,
    val tag_string_artist: String,
    val tag_string_character: String,
    val source: String?,
    val score: Int,
    val created_at: String,
    val updated_at: String
) {
    fun toApi(): DanbooruApiPost = DanbooruApiPost(
        id = id,
        rating = rating,
        imageWidth = image_width,
        imageHeight = image_height,
        isPending = is_pending,
        fileExt = file_ext,
        file_url,
        large_file_url,
        preview_file_url,
        parent_id,
        has_children,
        tag_string,
        tag_string_general,
        tag_string_meta,
        tag_string_copyright,
        tag_string_artist,
        tag_string_character,
        source,
        score,
        created_at,
        updated_at
    )
}